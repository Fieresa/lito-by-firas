/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;
import IService.IService;
import Entité.Education;
import Utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author khalf
 */
public class ServiceEducation implements IService<Education> {
    private Connection con;
    private Statement ste;

    public ServiceEducation() {
        con = DataBase.getInstance().getConnection();

    }

    @Override
    public void ajouter(Education t) throws SQLException {
        String req = "INSERT INTO education (nom,adresse,numtel,nbplaces,sujet) "
                + "VALUES ( ?,?,?,?,?) ";
        PreparedStatement pre = con.prepareStatement(req);
        pre.setString(1, t.getNom());
        pre.setString(2, t.getAdresse());
        pre.setString(3, t.getNumtel());
        pre.setInt(4, t.getNbplaces());
        pre.setString(5, t.getSujet());
        pre.executeUpdate();
     }
    
    @Override
    public boolean delete(Education t) throws SQLException {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `lito`.`education` where nom =?");
        pre.setString(1, t.getNom());
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("A user was deleted successfully!");
        }
        return true;
    }

    @Override
    public boolean update(String nom,String adresse,String numtel,int nbplaces,String sujet,Education p) throws SQLException {
        String sql = "UPDATE education SET nom=?, adresse=?, numtel=?, nbplaces=?, sujet=? WHERE nom=?";

        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, nom);
        statement.setString(2, adresse);
        statement.setString(3, numtel);
        statement.setInt(4, nbplaces);
        statement.setString(5, sujet);        
        statement.setString(6, p.getNom());
        

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing user was updated successfully!");
        }
        return true;
    }

    @Override
    public List<Education> readAll() throws SQLException {
        List<Education> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from education");
        while (rs.next()) {
            String nom = rs.getString("nom");
            String adresse = rs.getString(2);
            String numtel = rs.getString(3);
            int nbplaces = rs.getInt("nbplaces");
            String sujet= rs.getString("sujet");
            Education p = new Education(nom, adresse, numtel, nbplaces,sujet);
            arr.add(p);
        }
        return arr;
    }
    
    @Override
    public List<Education> getByName(String n) throws SQLException {
        Education a = null;
        List<Education> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet res ;
        res = ste.executeQuery(" select * from education where (nom like '%"+n+"%')");


            while (res.next())
            {
                a=new Education(res.getString(1),res.getString(2),res.getString(3),res.getInt(4),res.getString(5));
                arr.add(a);
            }

        return arr;
    }

    @Override
    public List<Education> getTrier() throws SQLException {
        List<Education> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from health ORDER BY nom DESC");
        while (rs.next()) {
            String nom = rs.getString("nom");
            String adresse = rs.getString(2);
            String numtel = rs.getString(3);
            int nbplaces = rs.getInt("nbplaces");
            String sujet= rs.getString("sujet");
            Education p = new Education(nom, adresse, numtel, nbplaces,sujet);
            arr.add(p);
        }
        return arr;
    }
    public List<Education> AdvTri(String t) throws SQLException{
        List<Education> arr = new ArrayList<>();
        ste = con.createStatement();
        String sql =("select * from education ORDER BY "+t);
        ResultSet rs = ste.executeQuery(sql);
        while (rs.next()) {
            String nom = rs.getString("nom");
            String adresse = rs.getString(2);
            String numtel = rs.getString(3);
            int nbplaces = rs.getInt("nbplaces");
            String sujet= rs.getString("sujet");
            Education p = new Education(nom, adresse, numtel, nbplaces,sujet);
            arr.add(p);
        }
        return arr;
    }

}
