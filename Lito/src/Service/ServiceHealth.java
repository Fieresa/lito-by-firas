/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entité.Health;
import IService.IHealthService;
import Utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author khalf
 */
public class ServiceHealth implements IHealthService<Health> {

    private Connection con;
    private Statement ste;

    public ServiceHealth() {
        con = DataBase.getInstance().getConnection();

    }

    @Override
    public void ajouterH(Health t) throws SQLException {
        String req = "INSERT INTO health (id,nom,prenom,adresse,specialite,numtel,etat) "
                + "VALUES (?,?,?,?,?,?,?) ";
        PreparedStatement pre = con.prepareStatement(req);
        pre.setString(1, t.getId());
        pre.setString(2, t.getNom());
        pre.setString(3, t.getPrenom());
        pre.setString(4, t.getAdresse());
        pre.setString(5, t.getSpecialite());
        pre.setString(6, t.getNumtel());
        pre.setString(7, t.getEtat());
        pre.executeUpdate();
    }

    @Override
    public boolean deleteH(Health t) throws SQLException {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `lito`.`health` where id =?");
        pre.setString(1, t.getId());
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("A user was deleted successfully!");
        }
        return true;
    }

    @Override
    public List<Health> readAllH() throws SQLException {
        List<Health> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from health");
        while (rs.next()) {
            String id = rs.getString("id");
            String adresse = rs.getString("adresse");
            String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            String specialite = rs.getString("specialite");
            String numtel = rs.getString("numtel");
            String etat = rs.getString("etat");
            Health p = new Health(id, nom, prenom, adresse, specialite, numtel, etat);
            arr.add(p);
        }
        return arr;
    }

    @Override
    public boolean updateH(String id, String nom, String prenom, String adresse, String specialite, String numtel, String etat, Health p) throws SQLException {

        String sql = "UPDATE education SET nom=?, adresse=?, numtel=?, nbplaces=?, sujet=? WHERE id=?";

        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, id);
        statement.setString(2, nom);
        statement.setString(3, prenom);
        statement.setString(4, adresse);
        statement.setString(5, specialite);
        statement.setString(6, numtel);
        statement.setString(7, etat);

        statement.setString(8, p.getId());

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing user was updated successfully!");
        }
        return true;
    }

    @Override
    public List<Health> getTrier() throws SQLException {
        List<Health> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from health ORDER BY id DESC");
        while (rs.next()) {
            String id = rs.getString("id");
            String adresse = rs.getString("adresse");
            String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            String specialite = rs.getString("specialite");
            String numtel = rs.getString("numtel");
            String etat = rs.getString("etat");
            Health p = new Health(id, nom, prenom, adresse, specialite, numtel, etat);
            arr.add(p);
        }
        return arr;
    }

    
    @Override
    public List<Health> getById(String n) throws SQLException {
        
        List<Health> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet res ;
        res = ste.executeQuery(" select * from health where (id like '%"+n+"%')");
            while (res.next())
            {
                Health a=new Health(res.getString(1),res.getString(2),res.getString(3),res.getString(4),res.getString(5),res.getString(6),res.getString(7));
                arr.add(a);
            }

        return arr;
    }
    
    @Override
    public List<Health> AdvTri(String t) throws SQLException{
        List<Health> arr = new ArrayList<>();
        ste = con.createStatement();
        String sql =("select * from health ORDER BY "+t);
        ResultSet rs = ste.executeQuery(sql);
        while (rs.next()) {
            String id = rs.getString("id");
            String adresse = rs.getString("adresse");
            String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            String specialite = rs.getString("specialite");
            String numtel = rs.getString("numtel");
            String etat = rs.getString("etat");
            Health p = new Health(id, nom, prenom, adresse, specialite, numtel, etat);
            arr.add(p);
        }
        return arr;
    }
    @Override
    public float availableD() throws SQLException{
          float s=0;
          float t=0;
           ste = con.createStatement();
           ResultSet rs1 = ste.executeQuery(" select count(*) from health" );
           while (rs1.next()) {
            t = rs1.getInt(1);
            }
             ResultSet rs = ste.executeQuery(" select count(*) from health  where etat like 'available' " );
           while (rs.next()) {
            s = rs.getInt(1);
            }
           float m=(float)((s/t)*100);

        return (m);
      }
}
