/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entité;

/**
 *
 * @author khalf
 */
public class Education {
    private String nom;
    private String adresse;
    private String numtel;
    private int nbplaces;
    private String sujet;

    public Education(String nom, String adresse, String numtel, int nbplaces, String sujet) {
        this.nom = nom;
        this.adresse = adresse;
        this.numtel = numtel;
        this.nbplaces = nbplaces;
        this.sujet = sujet;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumtel() {
        return numtel;
    }

    public void setNumtel(String numtel) {
        this.numtel = numtel;
    }

    public int getNbplaces() {
        return nbplaces;
    }

    public void setNbplaces(int nbplaces) {
        this.nbplaces = nbplaces;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    @Override
    public String toString() {
        return "Education{" + "nom=" + nom + ", adresse=" + adresse + ", numtel=" + numtel + ", nbplaces=" + nbplaces + ", sujet=" + sujet + '}';
    }
    
    
}
