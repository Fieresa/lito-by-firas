/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lito;

import Entité.Education;
import Entité.Health;
import Service.ServiceEducation;
import Service.ServiceHealth;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author khalf
 */
public class Lito {

    public static void main(String[] args) throws SQLException {
        ServiceEducation ser = new ServiceEducation();
        ServiceHealth ser2 = new ServiceHealth();
        Education e1 = new Education("Esprit", "El Ghazela", "12121", 5, "IT");
        Health e2 = new Health("3256412", "Khalfaoui", "Firas", "Mourouj", "Therapist", "28340472", "Available");
        ser2.ajouterH(e2);
        ser.ajouter(e1);
        Health e3 = new Health("6256812", "Khalfaoui", "Fieresa", "Mourouj", "Therapist", "28340472", "Unavailable");
        ser2.ajouterH(e3);

        try {
            List<Education> list = ser.readAll();
            List<Health> list2 = ser2.readAllH();
            System.out.println(list);
            System.out.println(list2);
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

}
