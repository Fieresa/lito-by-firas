/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IService;

import Entité.Education;
import Entité.Health;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author khalf
 * @param <T>
 */
public interface IService<T> {
    void ajouter(T t) throws SQLException;
    boolean delete(T t) throws SQLException;
    boolean update(String nom,String adresse,String numtel,int nbplaces,String sujet,Education p) throws SQLException;
    List<T> readAll() throws SQLException;
    List<T> getTrier() throws SQLException; 
    List<T> getByName(String n) throws SQLException;

}

