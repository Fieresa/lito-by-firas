/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IService;

import Entité.Education;
import Entité.Health;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author khalf
 * @param <T>
 */
public interface IHealthService<T> {
    void ajouterH(T t) throws SQLException;
    boolean deleteH(T t) throws SQLException;
    boolean updateH(String id,String nom,String prenom,String adresse,String specialite,String numtel,String etat, Health p) throws SQLException;
    List<T> readAllH() throws SQLException;
    List<T> getTrier() throws SQLException ;
    List<T> getById(String n) throws SQLException;
    List<T> AdvTri(String t)throws SQLException;
    float availableD() throws SQLException;
}
