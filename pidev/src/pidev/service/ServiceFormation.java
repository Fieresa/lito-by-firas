/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import pidev.IService.InterfaceFormation;
import pidev.base.BaseDonnee;
import pidev.entite.Formation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *    
 * @author DELL
 */
public class ServiceFormation implements InterfaceFormation<Formation> {
    private Connection con;
    private Statement ste;

    public ServiceFormation() {
        con = BaseDonnee.getInstance().getConnection();
    }

    @Override
    public void ajouter(Formation x) throws SQLException {
         ste = con.createStatement();
        String requeteInsert = "INSERT INTO `test2`.`formation` (`id_for`,`adresse`,`duree_mois`) VALUES ('" + x.getAdresse()+ "', '" + x.getDuree_mois()+ "');";
        ste.executeUpdate(requeteInsert);
    }

    @Override
    public boolean delete(Formation x) throws SQLException {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `test2`.`formation` where id_for =?");
        pre.setInt(1, x.getId_for());
        pre.executeUpdate(); 
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("A training was deleted successfully!");
        }
        return true;
    }

    @Override
    public boolean update(String adresse, int duree_mois, Formation f) throws SQLException {
        String sql = "UPDATE formation SET adresse=?, duree_mois=? WHERE id_for=?";

        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, adresse) ;
        statement.setInt(2, duree_mois);
        statement.setInt(3, f.getId_for());

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing training was updated successfully!");
        }
        return true;
    }

    @Override
    public List readAll() throws SQLException {
        List<Formation> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from formation");
        while (rs.next()) {
            int id_for = rs.getInt(1);
            String adresse = rs.getString(2);
            int duree_mois = rs.getInt(3);
            
            Formation p = new Formation(id_for, adresse, duree_mois);
            arr.add(p);
        }
        return arr;
    }
    
    public Formation rechercheFormation(String adr) {
          Formation a = null;
         String requete = " select* from demandeemploi  where (libelle like '%"+adr+"%')" ;
        try {
           
            ste = (Statement) con.createStatement();
         ResultSet   res =ste.executeQuery(requete);
            if (res.next())
            {a=new Formation(res.getInt(1),res.getString(2),res.getInt(3));}
        } catch (SQLException ex) {
            Logger.getLogger(ServiceFormation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a ;

}
