/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.service;

//import java.sql.Connection;
//import java.sql.Date;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;                                                             
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pidev.IService.InterfaceOpportunite;
import pidev.base.BaseDonnee;
import pidev.entite.DemandeEmploi;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author DELL
 */
public class ServiceDemande implements InterfaceOpportunite <DemandeEmploi> {
    
    private Connection con ;
    private Statement ste;

    public ServiceDemande() {
        con = (Connection) BaseDonnee.getInstance().getConnection();
    }
    
    
    
    @Override
    public void ajouter(DemandeEmploi t) throws SQLException {
        ste = (Statement) con.createStatement();
        String requeteInsert = "INSERT INTO `test2`.`demandeemploi` (`id_demande`,`date`,`libelle`) VALUES (NULL, '" + t.getDate()+ "', '" + t.getLib()+ "');";
        ste.executeUpdate(requeteInsert);
    }
    
//    public void ajouter(DemandeEmploi t) throws SQLException {
//        PreparedStatement pre = con.prepareStatement("INSERT INTO `test2`.`demandeemploi` ( `id_demande`, `date`, `libelle`) VALUES ( ?, ?, ?);");
//        pre.setInt(1, t.getIddem());
//        pre.setString(2, t.getDate());
//        pre.setString(3, t.getLib());
//        pre.executeUpdate();
//    }


    @Override
    public boolean delete(DemandeEmploi t) throws SQLException {
        PreparedStatement pre = (PreparedStatement) con.prepareStatement("DELETE FROM `test2`.`demandeemploi` where libelle =?");
        pre.setString(1, t.getLib());
        pre.executeUpdate();
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("An application was deleted successfully!");
        }
        return true;
    }

    @Override
    public boolean update(String date, String libelle, DemandeEmploi d) throws SQLException {
        String sql = "UPDATE demandeemploi SET date=?, libelle=? WHERE libelle=?";

        PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
        statement.setString(1, date) ;
        statement.setString(2, libelle);
        statement.setString(3, d.getLib());

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing application was updated successfully!");
        }
        return true;
    }

    @Override
    public List<DemandeEmploi> readAll() throws SQLException {
        List<DemandeEmploi> arr = new ArrayList<>();
        ste = (Statement) con.createStatement();
        ResultSet rs = ste.executeQuery("select * from demandeemploi");
        while (rs.next()) {
            int id_demande = rs.getInt(1);
            String date = rs.getString(2);
            String libelle = rs.getString(3);
            
            DemandeEmploi p = new DemandeEmploi(id_demande, date, libelle);
            arr.add(p);
        }
        return arr;
    }

   public DemandeEmploi rechercheBylibel(String lib) {
          DemandeEmploi a = null;
         String requete = " select* from demandeemploi  where (libelle like '%"+lib+"%')" ;
        try {
           
            ste = (Statement) con.createStatement();
         ResultSet   res =ste.executeQuery(requete);
            if (res.next())
            {a=new DemandeEmploi(res.getInt(1),res.getString(2),res.getString(3));}
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDemande.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a ;
   } 
    
    
}
