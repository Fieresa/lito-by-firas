/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pidev.IService.InterfaceOffre;
import pidev.base.BaseDonnee;
//import pidev.entite.DemandeEmploi;
import pidev.entite.OffreEmploi;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class ServiceOffre implements InterfaceOffre<OffreEmploi>{
    private Connection con;
    private Statement ste;

    public ServiceOffre() {
        con = BaseDonnee.getInstance().getConnection();
    }

    @Override
    public void ajouter(OffreEmploi t) throws SQLException {
        ste = con.createStatement();
        String requeteInsert = "INSERT INTO `test2`.`offreemploi` (`id_off`,`date`,`description` ) VALUES ('" + t.getDate()+ "', '" + t.getDescription()+ "');";
        ste.executeUpdate(requeteInsert);
    }

    @Override
    public boolean delete(OffreEmploi t) throws SQLException {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `test2`.`offreemploi` where id_off =?");
        pre.setInt(1, t.getId_off());
        pre.executeUpdate();
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("An offer was deleted successfully!");
        }
        return true;
    } 

    @Override
    public boolean update(String date, String description, OffreEmploi of) throws SQLException {
        String sql = "UPDATE offreemploi SET date=?, description=? WHERE id_off=?";

        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, date) ;
        statement.setString(2, description);
        statement.setInt(3, of.getId_off());

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing offer was updated successfully!");
        }
        return true;
    }

    @Override
    public List<OffreEmploi> readAll() throws SQLException {
         List<OffreEmploi> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from offreemploi");
        while (rs.next()) {
            int id_off= rs.getInt(1);
            String date = rs.getString(2);
            String description = rs.getString(3);
            
            OffreEmploi p = new OffreEmploi(id_off, date, description);
            arr.add(p);
        }
        return arr;
    }
    public OffreEmploi rechercheOffre(String desc) {
          OffreEmploi a = null;
         String requete = " select* from offreemploi  where (libelle like '%"+desc+"%')" ;
        try {
           
            ste = (Statement) con.createStatement();
         ResultSet   res =ste.executeQuery(requete);
            if (res.next())
            {a=new OffreEmploi(res.getInt(1),res.getString(2),res.getString(3));}
        } catch (SQLException ex) {
            Logger.getLogger(ServiceOffre.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a ;
    }
}
