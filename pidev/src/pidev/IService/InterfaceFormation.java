/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.IService;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
//import pidev.entite.DemandeEmploi;
import pidev.entite.Formation;

 


public interface InterfaceFormation<F> {
    void ajouter(F x) throws SQLException;
    boolean delete(F x) throws SQLException;
    boolean update( String adresse, int duree_mois,  Formation f) throws SQLException;
    List<F> readAll() throws SQLException;
    
}
