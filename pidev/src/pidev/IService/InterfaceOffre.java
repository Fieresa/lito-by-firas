/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.IService;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import pidev.entite.Formation;
import pidev.entite.OffreEmploi;

/**  int id_off ;
   String date ; 
    String description ; 
 *
 * @author DELL
 */
public interface InterfaceOffre<O> {
    
    void ajouter(O t) throws SQLException;
    boolean delete(O t) throws SQLException;
    boolean update(String date, String description, OffreEmploi of) throws SQLException;
    List<O> readAll() throws SQLException;
    
}
