/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.IService;
//import java.sql.Date;
import pidev.entite.DemandeEmploi;
//import pidev.entite.OffreEmploi;
//import pidev.entite.Formation;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author DELL
 */
public interface InterfaceOpportunite<T> {
    void ajouter(T t) throws SQLException;
    boolean delete(T t) throws SQLException;
    boolean update(String date, String libelle, DemandeEmploi d) throws SQLException;
    List<T> readAll() throws SQLException;
    
}
