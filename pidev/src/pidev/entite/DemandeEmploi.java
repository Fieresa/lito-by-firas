/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.entite;

import java.util.Date;

/**
 *
 * @author DELL
 */
public class DemandeEmploi {
    
    int id_demande ; 
   String date ;  
    String libelle ; 
    
    
    public DemandeEmploi(int id_demande,String date, String libelle) {
        this.id_demande = id_demande; 
        this.date = date;
        this.libelle = libelle;
       
    }
    public DemandeEmploi( String date, String libelle) {
       
        this.date = date;
        this.libelle = libelle;
         
    }

    

    public int getIddem() {
        return id_demande;
    }

    public String getDate() {
        return date;
    }
    public String getLib(){
    return libelle; 
    }
    

    public void setIddem(int id_demande) {
        this.id_demande = id_demande;
    }

    public void setDate( String date) {
        this.date = date;
    }
    public void setLib(String libelle) {
        this.libelle = libelle;
    }

    
    @Override
    public String toString() {
        return "DemandeEmploi{" + "id_demande=" + id_demande + ", date=" + date + ", libelle=" + libelle + '}';
    }
    
    
}
