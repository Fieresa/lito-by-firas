
package pidev.entite;

import java.util.Date;

public class OffreEmploi {
    
    int id_off ;
  String date ; 
    String description ; 

    public OffreEmploi(int id_off,String date, String description) {
        this.id_off = id_off;
        this.date = date;
        this.description = description;
    }

    public OffreEmploi(String date, String description) {
        this.date = date;
        this.description = description;
    }

    public int getId_off() {
        return id_off;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public void setId_off(int id_off) {
        this.id_off = id_off;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "OffreEmploi{" + "id_off=" + id_off + ", date=" + date + ", description=" + description + '}';
    }
    
    
}
