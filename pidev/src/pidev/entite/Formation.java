/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.entite;

/**
 *
 * @author DELL
 */
public class Formation {
    int id_for; 
    String adresse ;
    int duree_mois ; 
     

    public Formation(int id_for, String adresse, int duree_mois) {
        this.id_for = id_for;
        this.adresse = adresse;
        this.duree_mois = duree_mois;
        
    }

    public Formation(String adresse, int duree_mois) {
        this.adresse = adresse;
        this.duree_mois = duree_mois;
        
    }

    public int getId_for() {
        return id_for;
    }

    public String getAdresse() {
        return adresse;
    }

    public int getDuree_mois() {
        return duree_mois;
    }

   

    public void setId_for(int id_for) {
        this.id_for = id_for;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setDuree_mois(int duree_mois) {
        this.duree_mois = duree_mois;
    }

    

    @Override
    public String toString() {
        return "Formation{" + "id_for=" + id_for + ", adresse=" + adresse + ", duree_mois=" + duree_mois + ", id_demande=" +  '}';
    }
    
    
 
}
