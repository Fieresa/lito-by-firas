/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev.base;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author DELL
 */
public class BaseDonnee {
    String url = "jdbc:mysql://localhost:3308/test2";
     String login = "root";
     String pwd = "";
    public  static BaseDonnee db;
    public Connection con;
    
    private BaseDonnee() {
         try {
             con=DriverManager.getConnection(url, login, pwd);
             System.out.println("connexion etablie");
         } catch (SQLException ex) {
             System.out.println(ex);
         }
    }
    
    public Connection  getConnection()
    {
    return con;
    }     
    public static BaseDonnee getInstance()
    {if(db==null)
        db=new BaseDonnee();
    return db;
    }     
     
    
}
