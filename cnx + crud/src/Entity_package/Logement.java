/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity_package;

/**
 *
 * @author Sharky
 */
public class Logement {
    private String adress,etat_log,type,description;
    private int id_house,capacite,nb_resident;

    public Logement(String adress, String etat_log, String type, String description, int id_house, int capacite, int nb_resident) {
        this.adress = adress;
        this.etat_log = etat_log;
        this.type = type;
        this.description = description;
        this.id_house = id_house;
        this.capacite = capacite;
        this.nb_resident = nb_resident;
    }

    public Logement(int id_house) {
        this.id_house = id_house;
    }

    
    public Logement(String adress, String etat_log, String type, String description, int capacite, int nb_resident) {
        this.adress = adress;
        this.etat_log = etat_log;
        this.type = type;
        this.description = description;
        this.capacite = capacite;
        this.nb_resident = nb_resident;
    }
    

    
    
    public void setId(int id_house) {
        this.id_house = id_house;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setEtat_log(String etat_log) {
        this.etat_log = etat_log;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public void setNb_resident(int nb_resident) {
        this.nb_resident = nb_resident;
    }

    public int getId_house() {
        return id_house;
    }

    public String getAdress() {
        return adress;
    }

    public String getEtat_log() {
        return etat_log;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public int getCapacite() {
        return capacite;
    }

    public int getNb_resident() {
        return nb_resident;
    }
    
    
    @Override
    public String toString() {
        return "Logement{" + "id_house=" + id_house + ", adress=" + adress + ", etat_log=" + etat_log + ", type=" + type + ", description=" + description + ", capacite=" + capacite + ", nb_resident=" + nb_resident + '}';
    }
    
}
