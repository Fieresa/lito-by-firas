/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity_package;

/**
 *
 * @author Sharky
 */
public class Famille_acc {
    private int id_famille;
    private String etat_fa,adress_fa,tel_fa,exigence_fa;

    public Famille_acc(int id_famille, String etat_fa, String adress_fa, String tel_fa, String exigence_fa) {
        this.id_famille = id_famille;
        this.etat_fa = etat_fa;
        this.adress_fa = adress_fa;
        this.tel_fa = tel_fa;
        this.exigence_fa = exigence_fa;
    }

    public Famille_acc(String etat_fa, String adress_fa, String tel_fa, String exigence_fa) {
        this.etat_fa = etat_fa;
        this.adress_fa = adress_fa;
        this.tel_fa = tel_fa;
        this.exigence_fa = exigence_fa;
    }

    public Famille_acc(int id_famille) {
        this.id_famille = id_famille;
    }

    public int getId_famille() {
        return id_famille;
    }

    public String getEtat_fa() {
        return etat_fa;
    }

    public String getAdress_fa() {
        return adress_fa;
    }

    public String getTel_fa() {
        return tel_fa;
    }

    public String getExigence_fa() {
        return exigence_fa;
    }

    public void setId_famille(int id_famille) {
        this.id_famille = id_famille;
    }

    public void setEtat_fa(String etat_fa) {
        this.etat_fa = etat_fa;
    }

    public void setAdress_fa(String adress_fa) {
        this.adress_fa = adress_fa;
    }

    public void setTel_fa(String tel_fa) {
        this.tel_fa = tel_fa;
    }

    public void setExigence_fa(String exigence_fa) {
        this.exigence_fa = exigence_fa;
    }

    @Override
    public String toString() {
        return "Famille_acc{" + "id_famille=" + id_famille + ", etat_fa=" + etat_fa + ", adress_fa=" + adress_fa + ", tel_fa=" + tel_fa + ", exigence_fa=" + exigence_fa + '}';
    }
    
    
}
