/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_package;


import Entity_package.Logement;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author Sharky
 */
public interface ILogement<T> {
    void ajouter1_log(T t) throws SQLException;
    boolean delete_log(T t) throws SQLException;
    boolean update_log(String adress,String etat_log,String type,String description,int capacite,int nb_resident,Logement t) throws SQLException;
    List<T> readAll_log() throws SQLException;
    List<T> tri_log(String t) throws SQLException; 
    List<T> recherche_log(String x,String t) throws SQLException;
    float stat_log() throws SQLException;
}
