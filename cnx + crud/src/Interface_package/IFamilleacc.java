/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface_package;

import Entity_package.Famille_acc;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Sharky
 */
public interface IFamilleacc<T> {
    void ajouter1_fa(T t) throws SQLException;
    boolean delete_fa(T t) throws SQLException;
    boolean update_fa(String etat_fa,String adress_fa,String tel_fa,String exigence_fa,Famille_acc t) throws SQLException;
    List<T> readAll_fa() throws SQLException;
}
