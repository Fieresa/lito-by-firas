/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service_package;
import Entity_package.Logement;
import Interface_package.ILogement;
import java.sql.SQLException;
import java.util.List;
import java.sql.*;
import Tools_package.Database;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Sharky
 */
public class Service_logement implements ILogement <Logement>{
    private Connection con;
    private Statement ste;

    public Service_logement() {
        con = Database.getInstance().getConnection();

    }

    /*@Override
    public void ajouter(Logement t) throws SQLException {
        ste = con.createStatement();
        String requeteInsert = "INSERT INTO `esprit1`.`Logement` (`id_house`, `adress`, `capacite`, `nb_resident`,'etat_log','type','description') VALUES (NULL, '" + t.getNom() + "', '" + t.getPrenom() + "', '" + t.getAge() + "');";
        ste.executeUpdate(requeteInsert);
    }*/

    public void ajouter1_log(Logement t) throws SQLException {
        PreparedStatement pre = con.prepareStatement("INSERT INTO `esprit1`.`Logement` (`adress`, `etat_log`,`type`,`description`,`capacite`,`nb_resident`) VALUES ( ?, ?, ?, ?, ?, ?);");
        pre.setString(1, t.getAdress());
        pre.setString(2, t.getEtat_log());
        pre.setString(3, t.getType());
        pre.setString(4, t.getDescription());
        pre.setInt(5, t.getCapacite());
        pre.setInt(6, t.getNb_resident());
        
        
        
        pre.executeUpdate();
    }

    @Override
    public boolean delete_log(Logement t) throws SQLException {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `esprit1`.`Logement` where id_house =?");
        pre.setInt(1, t.getId_house());
        pre.executeUpdate();
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("A Shelter was deleted successfully!");
        }
        return true;
    }

    @Override
    public boolean update_log(String adress,String etat_log,String type,String description,int capacite,int nb_resident,Logement t) throws SQLException {
        String sql = "UPDATE Logement SET adress=?, etat_log=?, type=? ,description=?, capacite=?, nb_resident=? WHERE Id_house=?";

        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, adress);
        statement.setString(2, etat_log);
        statement.setString(3, type);
        statement.setString(4, description);
        statement.setInt(5, capacite);
        statement.setInt(6, nb_resident);
        statement.setInt(7, t.getId_house());

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing user was updated successfully!");
        }
        return true;
    }

    @Override
    public List<Logement> tri_log(String t) throws SQLException {
        List<Logement> arr = new ArrayList<>();
        ste = con.createStatement();
        String sql =("select * from Logement ORDER BY "+t);
        ResultSet rs = ste.executeQuery(sql);
        while (rs.next()) {
            String adress = rs.getString("adress");
            String etat_log = rs.getString("etat_log");
            String type = rs.getString("type");
            String description = rs.getString("description");
            int id_house = rs.getInt("id_house");
            int capacite = rs.getInt("capacite");
            int nb_resident = rs.getInt("nb_resident");
            Logement p = new Logement(adress,etat_log,type,description,id_house,capacite,nb_resident);
            arr.add(p);
        }
        return arr;
    }  
    
        @Override
    public List<Logement> readAll_log() throws SQLException {
        List<Logement> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from Logement ");
        while (rs.next()) {
            String adress = rs.getString("adress");
            String etat_log = rs.getString("etat_log");
            String type = rs.getString("type");
            String description = rs.getString("description");
            int id_house = rs.getInt("id_house");
            int capacite = rs.getInt("capacite");
            int nb_resident = rs.getInt("nb_resident");
            Logement p = new Logement(adress,etat_log,type,description,id_house,capacite,nb_resident);
            arr.add(p);
        }
        return arr;
    }
    
    @Override
    public List<Logement> recherche_log(String x,String t) throws SQLException{
        Logement a = null;
        List<Logement> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select* from Logement  where ("+t+" like '%" + x + "%')");
        while (rs.next()) {
            int id_house = rs.getInt("id_house");
            String adress = rs.getString("adress");
            String etat_log = rs.getString("etat_log");
            String type = rs.getString("type");
            String description = rs.getString("description");
            int capacite = rs.getInt("capacite");
            int nb_resident = rs.getInt("nb_resident");
            a = new Logement(adress, etat_log, type, description, id_house, capacite, nb_resident);
            arr.add(a);
        }
        return arr;
    }
    public float stat_log() throws SQLException{
    float s=0;
          float t=0;
           ste = con.createStatement();
           ResultSet rs1 = ste.executeQuery(" select sum(nb_resident) from Logement" );
           while (rs1.next()) {
            t = rs1.getInt(1);
            }
             ResultSet rs = ste.executeQuery(" select sum(capacite) from logement " );
           while (rs.next()) {
            s = rs.getInt(1);
            }
           float m=(float)((s/t)*100);

        return (m);
    }
    
}
