/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service_package;

import Entity_package.Famille_acc;
import Entity_package.Logement;
import Interface_package.IFamilleacc;
import Tools_package.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sharky
 */
    public class Service_famille implements IFamilleacc <Famille_acc>{
    private Connection con;
    private Statement ste;

    public Service_famille() {
        con = Database.getInstance().getConnection();

    }

    /*@Override
    public void ajouter(Logement t) throws SQLException {
        ste = con.createStatement();
        String requeteInsert = "INSERT INTO `esprit1`.`Logement` (`id_house`, `adress`, `capacite`, `nb_resident`,'etat_log','type','description') VALUES (NULL, '" + t.getNom() + "', '" + t.getPrenom() + "', '" + t.getAge() + "');";
        ste.executeUpdate(requeteInsert);
    }*/

    public void ajouter1_fa(Famille_acc t) throws SQLException {
        PreparedStatement pre = con.prepareStatement("INSERT INTO `esprit1`.`Famille_accueil` (`etat_fa`, `adress_fa`,`tel_fa`,`exigence_fa`) VALUES ( ?, ?, ?, ?);");
        pre.setString(1, t.getEtat_fa());
        pre.setString(2, t.getAdress_fa());
        pre.setString(3, t.getTel_fa());
        pre.setString(4, t.getExigence_fa());
        pre.executeUpdate();
    }

    @Override
    public boolean delete_fa(Famille_acc t) throws SQLException {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `esprit1`.`Famille_accueil` where id_famille =?");
        pre.setInt(1, t.getId_famille());
        pre.executeUpdate();
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("A Shelter was deleted successfully!");
        }
        return true;
    }

    @Override
    public boolean update_fa(String etat_fa,String adress_fa,String tel_fa,String exigence_fa,Famille_acc t) throws SQLException {
        String sql = "UPDATE famille_accueil SET etat_fa=?, adress_fa=? ,tel_fa=?, exigence_fa=? WHERE Id_famille=?";

        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, etat_fa);
        statement.setString(2, adress_fa);
        statement.setString(3, tel_fa);
        statement.setString(4, exigence_fa);
        statement.setInt(5, t.getId_famille());

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing user was updated successfully!");
        }
        return true;
    }

    @Override
    public List<Famille_acc> readAll_fa() throws SQLException {
        List<Famille_acc> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from Famille_accueil");
        while (rs.next()) {
            int id_famille = rs.getInt("id_famille");
            String etat_fa = rs.getString("etat_fa");
            String adress_fa = rs.getString("adress_fa");
            String tel_fa = rs.getString("tel_fa");
            String exigence_fa = rs.getString("exigence_fa");
            Famille_acc p = new Famille_acc(etat_fa,adress_fa,tel_fa,exigence_fa);
            arr.add(p);
        }
        return arr;
    }   
    }
