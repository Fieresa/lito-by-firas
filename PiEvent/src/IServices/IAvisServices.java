/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IServices;

import Entities.Avis;
import Entities.Event;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author fallag firas
 * @param <T>
 */
public interface IAvisServices<T> {
    public void ajouter(T e) throws SQLException ;
    public boolean delete(T e) throws SQLException;
    public boolean update(Date Date_avis, String Content_avis, String Reponse_avis, Avis e) throws SQLException;
    List<T> readAll() throws SQLException;
    //public List<Event> Event(Date date_debut , Date date_fin) throws SQLException;
    
}
