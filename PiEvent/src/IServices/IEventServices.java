/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IServices;

import Entities.Event;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author fallag firas
 * @param <T>
 */
public interface IEventServices<T> {
    
    public void ajouter(T e) throws SQLException ;
    public boolean delete(T e) throws SQLException;
    public boolean update(String Nom, String Type, Date date, String Etat, String Description, T e) throws SQLException;
    public List<T> readAll(String n) throws SQLException;
    public List<Event> searchEvent(Date date_debut , Date date_fin) throws SQLException;
    //public Event getByName(String n);
    public List<T> getByName(String n) throws SQLException;
    
    
    
}
