/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Avis;
import Entities.Event;
import Utils.DataBase;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fallag firas
 */
public class ServiceAvis implements IServices.IAvisServices<Avis> {
    private final Connection con= DataBase.getInstance().getConnection();;
    private Statement ste;
    
    /**
     *
     * @param e
     * @throws SQLException
     */
    @Override
    public void ajouter(Avis e) throws SQLException {
        String req = "INSERT INTO `avis` (`id_EventFK`,`Date_avis`,`Content_avis`,`Reponse_avis`) "
                + "VALUES ( ?,?,?,?) ";
        PreparedStatement pre = con.prepareStatement(req);
        pre.setInt(1, e.getId_EventFK());
        pre.setDate(2, e.getDate_avis());
        pre.setString(3, e.getContent_avis());
        pre.setString(4, e.getReponse_avis());
        pre.executeUpdate();
    }
    @Override
    public boolean delete(Avis e) throws SQLException
    {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `avis` where Id_avis =?");
        pre.setInt(1, e.getId_avis());
        pre.executeUpdate();
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("An event was deleted successfully!");
        }
        return true;
    }
    
    
    
    @Override
    public boolean update(Date Date_avis, String Content_avis, String Reponse_avis, Avis e) throws SQLException
    {
        String sql = "UPDATE avis SET Date_avis=?, Content_avis=?, Reponse_avis=? WHERE Id_avis=?";
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setDate(1, Date_avis);
        statement.setString(2, Content_avis);
        statement.setString(3, Reponse_avis);
        statement.setInt(4, e.getId_avis());

        int rowsUpdated = statement.executeUpdate();  
        if (rowsUpdated > 0) {
            System.out.println("An existing event was updated successfully!");
        }
        return true;
    }
    
    
    
     @Override
    public List<Avis> readAll() throws SQLException {
        List<Avis> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from avis");
        while (rs.next()) {
            int Id_avis = rs.getInt(1);
            int id_EventFK = rs.getInt(2);
            Date Date_avis = rs.getDate(3);
            String Content_avis = rs.getString(4);
            String Reponse_avis = rs.getString(5);
            Avis a = new Avis(Id_avis, id_EventFK, Date_avis, Content_avis, Reponse_avis);
            arr.add(a);
        }
        return arr;
    
    
}
}