/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

/**
 *
 * @author fallag firas
 */



import Entities.Event;
import Utils.DataBase;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceEvent implements IServices.IEventServices<Event> {
    private Connection con= DataBase.getInstance().getConnection();;
    private Statement ste;
    
    
    
    @Override
    public void ajouter(Event e) throws SQLException {
        String req = "INSERT INTO `events` (`Nom`,`Type`,`Date`,`Etat`,`Description`) "
                + "VALUES ( ?,?,?,?,?) ";
        PreparedStatement pre = con.prepareStatement(req);
        pre.setString(1, e.getNom());
        pre.setString(2, e.getType());
        pre.setDate(3, e.getDate());
        pre.setString(4, e.getEtat());
        pre.setString(5, e.getDescription());
        pre.executeUpdate();
     }   
     
     @Override
    public boolean delete(Event e) throws SQLException {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `events` where Id =?");
        pre.setInt(1, e.getIdE());
        pre.executeUpdate();
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("An event was deleted successfully!");
        }
        return true;
    }
     
     @Override
    public boolean update(String Nom, String Type, Date date, String Etat, String Description, Event e) throws SQLException {
        String sql = "UPDATE events SET Nom=?, Type=?, Date=?, Etat=?, Description=? WHERE Id=?";
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, Nom);
        statement.setString(2, Type);
        statement.setDate(3, date);
        statement.setString(4, Etat);
        statement.setString(5, Description);
        statement.setInt(6, e.getIdE());

        int rowsUpdated = statement.executeUpdate();  
        if (rowsUpdated > 0) {
            System.out.println("An existing event was updated successfully!");
        }
        return true;
    }
     
     @Override
    public List<Event> readAll(String n) throws SQLException {
        List<Event> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from events ORDER by "+n+" DESC");
        while (rs.next()) {
            int id = rs.getInt(1);
            String Nom = rs.getString(2);
            String Type = rs.getString(3);
            Date date = rs.getDate(4);
            String Etat = rs.getString(5);
            String Description = rs.getString(6);
            Event e = new Event(id, Nom, Type, date, Etat, Description);
            arr.add(e);
        }
        return arr;
    }
    
    @Override
    public List<Event> searchEvent(Date date_debut , Date date_fin ) throws SQLException{ 
        List<Event> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("SELECT * FROM events WHERE Date BETWEEN '"+date_debut+"' AND '"+date_fin+"'  ");
        while (rs.next()) {
            int id = rs.getInt(1);
            String Nom = rs.getString(2);
            String Type = rs.getString(3);
            Date date = rs.getDate(4);
            String Etat = rs.getString(5);
            String Description = rs.getString(6);
            
            
            //if(date_debut.compareTo(date) < 0 && date_fin.compareTo(date) > 0)
            //{
                Event e = new Event(id, Nom, Type, date, Etat, Description);
                arr.add(e);
            //}
        }
        return arr;
    }
    
    
    
    /*@Override
    public Event getByName(String n) {
          Event a = null;
         String requete = " select* from events where (Nom like '%"+n+"%')" ;
        try {
            ResultSet res;
            ste = con.createStatement();
            res=ste.executeQuery(requete);
            if (res.next())
            {a=new Event(res.getInt(1),res.getString(2),res.getString(3),res.getDate(4),res.getString(5),res.getString(6));}
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a ;
    }*/
    
    
    @Override
    public List<Event> getByName(String n) throws SQLException {         
        Event a = null;         
        List<Event> arr = new ArrayList<>();        
        ste = con.createStatement();
        ResultSet res ;
        res = ste.executeQuery(" select* from events where (Nom like '%"+n+"%')");
        
            
            while (res.next())
            {
                a=new Event(res.getInt(1),res.getString(2),res.getString(3),res.getDate(4),res.getString(5),res.getString(6));
                arr.add(a);
            }
        
        return arr;
    }
    
     
     
     
     
     
     
     
     
}
