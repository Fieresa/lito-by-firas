/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;

/**
 *
 * @author fallag firas
 */
public class Event {
    public int IdE;
    public String Nom;
    public String Type;
    public Date date;
    public String Etat;
    public String Description;

    public Event(int IdE, String Nom, String Type, Date date, String Etat, String Description) {
        this.IdE = IdE;
        this.Nom = Nom;
        this.Type = Type;
        this.date = date;
        this.Etat = Etat;
        this.Description = Description;
    }

    public Event(String Nom, String Type, Date date, String Etat, String Description) {
        this.Nom = Nom;
        this.Type = Type;
        this.date = date;
        this.Etat = Etat;
        this.Description = Description;
    }

    public Event() {
    }

    public Event(int IdE) {
        this.IdE = IdE;
    }

    public int getIdE() {
        return IdE;
    }

    public void setIdE(int IdE) {
        this.IdE = IdE;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEtat() {
        return Etat;
    }

    public void setEtat(String Etat) {
        this.Etat = Etat;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    @Override
    public String toString() {
        return "Event{" + "IdE=" + IdE + ", Nom=" + Nom + ", Type=" + Type + ", date=" + date + ", Etat=" + Etat + ", Description=" + Description + '}';
    }
    
    
    
    
}
