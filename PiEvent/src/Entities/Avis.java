/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;

/**
 *
 * @author fallag firas
 */
public class Avis {
    public int id_avis;
    public int id_EventFK;
    public Date Date_avis;
    public String Content_avis;
    public String reponse_avis;

    public Avis(int id_EventFK, Date Date_avis, String Content_avis, String reponse_avis) {
        this.id_EventFK = id_EventFK;
        this.Date_avis = Date_avis;
        this.Content_avis = Content_avis;
        this.reponse_avis = reponse_avis;
    }

    public Avis(int id_avis, int id_EventFK, Date Date_avis, String Content_avis, String reponse_avis) {
        this.id_avis = id_avis;
        this.id_EventFK = id_EventFK;
        this.Date_avis = Date_avis;
        this.Content_avis = Content_avis;
        this.reponse_avis = reponse_avis;
    }

    public Avis(int id_avis) {
        this.id_avis = id_avis;
    }

    public Avis(Date Date_avis, String Content_avis, String reponse_avis) {
        this.Date_avis = Date_avis;
        this.Content_avis = Content_avis;
        this.reponse_avis = reponse_avis;
    }

    public int getId_avis() {
        return id_avis;
    }

    public void setId_avis(int id_avis) {
        this.id_avis = id_avis;
    }

    public int getId_EventFK() {
        return id_EventFK;
    }

    public void setId_EventFK(int id_EventFK) {
        this.id_EventFK = id_EventFK;
    }

    public Date getDate_avis() {
        return Date_avis;
    }

    public void setDate_avis(Date Date_avis) {
        this.Date_avis = Date_avis;
    }

    public String getContent_avis() {
        return Content_avis;
    }

    public void setContent_avis(String Content_avis) {
        this.Content_avis = Content_avis;
    }

    public String getReponse_avis() {
        return reponse_avis;
    }

    public void setReponse_avis(String reponse_avis) {
        this.reponse_avis = reponse_avis;
    }

    @Override
    public String toString() {
        return "Avis{" + "id_avis=" + id_avis + ", id_EventFK=" + id_EventFK + ", Date_avis=" + Date_avis + ", Content_avis=" + Content_avis + ", reponse_avis=" + reponse_avis + '}';
    }
    
    
    
    
}
