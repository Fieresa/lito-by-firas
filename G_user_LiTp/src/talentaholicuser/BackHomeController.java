/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package talentaholicuser;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Meriem
 */
public class BackHomeController implements Initializable {

    @FXML
    private TableView<?> tableview;
    @FXML
    private TableColumn<?, ?> id;
    @FXML
    private TableColumn<?, ?> username;
    @FXML
    private TableColumn<?, ?> email;
    @FXML
    private TableColumn<?, ?> password;
    @FXML
    private TableColumn<?, ?> accounttype;
    @FXML
    private Button delete;
    @FXML
    private Button desactivate;
    @FXML
    private Button add;
    @FXML
    private Button generatepdf;
    @FXML
    private TextField identifiant;
    @FXML
    private Button find;
    @FXML
    private TextField username_text;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void delete(ActionEvent event) {
    }

    @FXML
    private void desactivate(ActionEvent event) {
    }

    @FXML
    private void adduser(ActionEvent event) {
    }

    @FXML
    private void generatepdf(ActionEvent event) {
    }

    @FXML
    private void find(ActionEvent event) {
    }
    
}
