/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Service;

import Lito.Entite.Donneur;
import Lito.Utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Meriem
 */
public class ServiceDonneur {
    
      private Connection con = DataBase.getInstance().getConnection();
    private Statement ste;
      public void ajouter(Donneur d) throws SQLException {
        String req = "INSERT INTO donneur (`rank`,`point`,`telephone`) "
                + "VALUES ( ?,?,?) ";
        PreparedStatement pre = con.prepareStatement(req);
        
  pre.setString(1, d.getRank()); 
        pre.setString(2, d.getPoint());
           
        pre.setString(3, d.getTelephone());
       
        // pre.setString(5, C.getMail());
    
   pre.executeUpdate();
     }
}
