/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Service;

import Lito.Entite.Candidat;
import Lito.Entite.User;
import Lito.Utils.DataBase;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.input.KeyCode;

/**
 *
 * @author Meriem
 */
public class ServiceCandidat {
      private Connection con = DataBase.getInstance().getConnection();
    private Statement ste;

    public void ajouter(Candidat C) throws SQLException {
        String req = "INSERT INTO candidat (`adresse`,`cin`,`telephone`,`niveau` ,`critere`) "
                + "VALUES ( ?,?,?,?,?) ";
        PreparedStatement pre = con.prepareStatement(req);
        
  pre.setString(1, C.getAdresse()); 
        pre.setString(2, C.getCIN());
           
        pre.setString(3, C.getTel());
        pre.setString(4, C.getNiveau());
           pre.setString(5, C.getCritere());
        // pre.setString(5, C.getMail());
    
   pre.executeUpdate();
     }
    
    
    
   
    public List<Candidat> readAll() throws SQLException {
        List<Candidat> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from candidat");
        while (rs.next()) {
            int idC = rs.getInt(1);
                String adresse = rs.getString("adresse");
            String CIN = rs.getString("CIN");
            String tel = rs.getString("telephone");
             String niveau = rs.getString("niveau");
              String critere = rs.getString("critere");
            Candidat C = new Candidat( adresse,CIN, tel, niveau,critere);
            arr.add(C);
        }
        return arr;
    }
    
      public boolean update( String adresse, String CIN, String tel, String niveau, String critere,Candidat C) throws SQLException {
        String sql = "UPDATE user SET adresse=?, CIN=?, telephone=? , niveau=? , critere=? WHERE idC=?";

        PreparedStatement statement = con.prepareStatement(sql);
         
       statement.setString(1, adresse);
        statement.setString(2, CIN);
         statement.setString(3, tel);
          statement.setString(4, niveau);
          statement.setString(5, critere);
       

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing Candidat was updated successfully!");
        }
        return true;
    }
        public List<Candidat> searchCandidat(String n) throws SQLException{ 
        List<Candidat> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("SELECT * FROM candidat WHERE critere ='"+n+"'  ");
        while (rs.next()) {
            int idC = rs.getInt(1);
            String adresse = rs.getString(2);
            String CIN = rs.getString(3);
            String tel = rs.getString(4);
            String niveau = rs.getString(5);
            String critere = rs.getString(6);
  
                Candidat ca = new Candidat(adresse,CIN, tel, niveau, critere);
                arr.add(ca);
            
        }
        return arr;
    }
        
          public List<Candidat> getTrier() throws SQLException {
    List<Candidat> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from candidat ORDER BY critere");
        while (rs.next()) {
            int idC = rs.getInt(1);
            String adresse = rs.getString("adresse");
            String CIN = rs.getString("CIN");
            String tel = rs.getString("tel");
            String niveau = rs.getString("niveau");
            String critere = rs.getString("critere");
  Candidat ca = new Candidat(adresse,CIN, tel, niveau, critere);
                arr.add(ca);     }
    return arr;
    }

    public void ajouter(KeyCode keyCode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public float stat() throws SQLException{
          float s=0;
          float t=0;
           ste = con.createStatement();
           ResultSet rs1 = ste.executeQuery(" select count(*) from candidat" );
           while (rs1.next()) {
            t = rs1.getInt(1);
            }
             ResultSet rs = ste.executeQuery(" select count(*) from candidat  where critere='benevole' " );
           while (rs.next()) {
            s = rs.getInt(1);
            }
           float m=(float)((s/t)*100);

        return (m);
      }
}
