/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Service;

import Lito.Entite.User;
import Lito.IService.IService;
import java.sql.SQLException;
import java.util.List;
import java.sql.*;
import Lito.Utils.DataBase;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;
import Lito.Utils.*;
import java.security.SecureRandom;

/**
 *
 * @author Meriem
 */
public class ServiceUser implements IService<User> { 
    
     private Connection con = DataBase.getInstance().getConnection();
    private Statement ste;

    
        public void registerC(User u) {
        

        try {
         Connection con=DataBase.getInstance().getConnection();

        ste = con.createStatement(); 
        String req = "INSERT INTO `user`(`nom`, `prenom`, `mail`, `password`, `role` ) VALUES ('" + u.getNom() + "','" + u.getPrenom() + "','" + u.getMail() + "','" + u.getPassword() + "','" + u.getRole() + "')";
        ste.executeUpdate(req);
        } catch (SQLException ex) {
            Logger.getLogger(ServiceUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 public boolean verifyPass(String nom, String password) {
         

        try {
             Connection con=DataBase.getInstance().getConnection();
             ste = con.createStatement(); 
            String req = "SELECT password from user where user.nom='" + nom + "' ";
            ResultSet res = ste.executeQuery(req);
            if (res.next()) {
                String passbd = res.getString(1);
                if (passbd.equals(password)) {
                    return true;
                }
                /*
                BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), passbd);
                if (result.verified) {
                    return true;
                }*/
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServiceUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
     
  public User seConnecter(String login) {
                
        try {
       Connection con=DataBase.getInstance().getConnection();
          ste = con.createStatement(); 
            String req = "SELECT id,nom,prenom,mail,password,role from user where user.nom='" + login + "' ";
            ResultSet res = ste.executeQuery(req);
            if (res.next()) {
                 int id = res.getInt("id");
                String nom = res.getString("nom");
                String prenom = res.getString("prenom");
                String mail = res.getString("mail");
                String password = res.getString("password");
                String role = res.getString("role");
             
                User u = new User(id, nom, prenom, mail, password, role);
                return u;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServiceUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
  public User login(String user , String mdp) throws SQLException{
     String sql = "SELECT * FROM user WHERE mail = ? and password = ?";
     Connection con=DataBase.getInstance().getConnection();
     PreparedStatement preparedStatement = con.prepareStatement(sql);
      preparedStatement.setString(1, user);
      preparedStatement.setString(2,mdp);
      ResultSet res = preparedStatement.executeQuery();
               if (res.next()) {
                 int id = res.getInt("id");
                String nom = res.getString("nom");
                String prenom = res.getString("prenom");
                String mail = res.getString("mail");
                String password = res.getString("password");
                String role = res.getString("role");
             
                User u = new User(id, nom, prenom, mail, password, role);
                return u;
            }
            else
             return null;
     
  }
  
  
  // à faire **************************************************
  /* public void confirmAccount(int id) {
           
        try {
   Connection con=DataBase.getInstance().getConnection();
 PreparedStatement ste = con.prepareStatement("UPDATE user SET active=1 WHERE id=?") ;
            ste.setInt(1, id);
            ste.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceUser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }*/
//************************************************************************
  
     public String resetPasswordFirst(int id) {
            

        try {
           Connection con=DataBase.getInstance().getConnection();
            codeGen code;
            code = new codeGen();
            String codeg = code.randomString(8);
            PreparedStatement st = con.prepareStatement("UPDATE user SET reset_token= ? WHERE id=?");
            st.setInt(2, id);
            st.setString(1, codeg);
            st.execute();
            return codeg;
        } catch (SQLException ex) {
            Logger.getLogger(ServiceUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
  
     @Override
    public void ajouter(User e) throws SQLException {
        String req = "INSERT INTO user (`nom`,`prenom`,`mail`,`password` ,`role`) "
                + "VALUES ( ?,?,?,?,?) ";
        PreparedStatement pre = con.prepareStatement(req);
        pre.setString(1, e.getNom());
        pre.setString(2, e.getPrenom());
        pre.setString(3, e.getMail());
        pre.setString(4, e.getPassword());
       pre.setString(5, e.getRole());
        pre.executeUpdate();
     }
 

   /*@Override
     public void ajouter(User u) throws SQLException {
        PreparedStatement pre = con.prepareStatement("INSERT INTO `user` ( `nom`, `prenom`, `mail`, `password`) VALUES ( ?, ?, ? ,? );");
        pre.setString(1, u.getNom());
        pre.setString(2, u.getPrenom());
       pre.setString(3, u.getMail());
       pre.setString(4, u.getPassword());
        pre.executeUpdate();
    }*/

    @Override
      public boolean delete(User t) throws SQLException {
        PreparedStatement pre = con.prepareStatement("DELETE FROM `lito`.`user` where nom =?");
        pre.setString(1, t.getNom());
        pre.executeUpdate();
        int rowsDeleted = pre.executeUpdate();
        if (rowsDeleted > 0) {
            System.out.println("A user was deleted successfully!");
        }
        return true;
    }
      

   @Override
     public List<User> readAll() throws SQLException {
        List<User> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from user");
        while (rs.next()) {
            int id = rs.getInt(1);
            String nom = rs.getString("nom");
            String prenom = rs.getString(3);
            String mail = rs.getString("mail");
             String password = rs.getString("password");
              String role = rs.getString("role");
            User u = new User(id, nom, prenom, mail, password,role);
            arr.add(u);
        }
        return arr;
    }
@Override
     public boolean update(String nom,String prenom,String mail, String password,String role,User u) throws SQLException {
        String sql = "UPDATE user SET nom=?, prenom=?, mail=? , password=? , role=? WHERE id=?";

        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1, nom);
        statement.setString(2, prenom);
         statement.setString(3, mail);
          statement.setString(4, password);
          statement.setString(5, role);
        statement.setInt(6, u.getId());

        int rowsUpdated = statement.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing user was updated successfully!");
        }
        return true;
    }

  @Override
     public List<User> searchUser(String n) throws SQLException{ 
        List<User> arr = new ArrayList<>();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("SELECT * FROM user WHERE nom ='"+n+"'  ");
        while (rs.next()) {
            int id = rs.getInt(1);
            String nom = rs.getString(2);
            String prenom = rs.getString(3);
            String mail = rs.getString(4);
            String password = rs.getString(5);
            String role = rs.getString(6);
  
                User e = new User(id, nom, prenom, mail, password, role);
                arr.add(e);
            
        }
        return arr;
    }
     
      public List<User> getTrier() throws SQLException {
    List<User> arr=new ArrayList<>();
    ste=con.createStatement();
    ResultSet rs=ste.executeQuery("select * from User ORDER BY nom DESC");
        while (rs.next()) {
            int id = rs.getInt(1);
            String nom = rs.getString("nom");
            String prenom = rs.getString("prenom");
            String mail = rs.getString("mail");
            String password = rs.getString("password");
            String role = rs.getString("role");
      
            User a = new User(id,nom,prenom,mail,password,role);
            arr.add(a);
     }
    return arr;
    }

    private static class codeGen {

      private static codeGen instance;
  
    
   
    
    public static codeGen getInstance(){
        if(instance==null) 
            instance=new codeGen();
        return instance;
    }

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public String randomString(int len) 
    {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }
    }
      
      
      
          public String CryptingPassword(String password) {
        String crypte="";
        for (int i=0; i<password.length();i++)  {
            int c=password.charAt(i)^48; 
            crypte=crypte+(char)c;
        }
        return crypte;
    }
    
    public String decrypt(String password){
        String aCrypter="";
        for (int i=0; i<password.length();i++)  {
            int c=password.charAt(i)^48; 
            aCrypter=aCrypter+(char)c;
        }
        return aCrypter;
    }
      public int GetIdUser(String mail,String password) throws SQLException
    {
        List<User> list =readAll();
        for(User u:list)
        {
            if((u.getMail().equals(mail))&&(decrypt(u.getPassword()).equals(password)))
            {
                return u.getId();
            }
        }
        return -1;
    }
   

  
   
    }
