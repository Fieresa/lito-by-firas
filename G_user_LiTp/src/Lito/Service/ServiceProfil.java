/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Service;

import Lito.Entite.profil;
import Lito.Utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Meriem
 */
public class ServiceProfil {
     private Connection con = DataBase.getInstance().getConnection();
    private Statement ste;

    

   public void ajouterProfil(profil p) {
        PreparedStatement usrPs;
        try {
            String usrQry = "INSERT INTO profil(user_id, profile_picture) "
                          + "VALUES (?,?)";
            usrPs = con.prepareStatement(usrQry);
            usrPs.setInt (1,p.getUser_id());
            usrPs.setString (2,p.getProfilePicURL());
            usrPs.executeUpdate();
        } catch (SQLException ex) {
       
        }
    }
 public ResultSet afficherProfil() {
         ResultSet rs = null;
        try {

            String SQL = "SELECT * from profil INNER JOIN fos_user on profile.user_id=fos_user.id";
            PreparedStatement ps;
            ps = con.prepareCall(SQL);
            rs = ps.executeQuery();
        }catch (SQLException ex) {
        }
        return rs;
    }
}

