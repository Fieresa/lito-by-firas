/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Service;
import Lito.Entite.Refuge;
import Lito.Utils.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Meriem
 */
public class ServiceRefuge {
      private Connection con = DataBase.getInstance().getConnection();
    private Statement ste;
      public void ajouter(Refuge ref) throws SQLException {
        String req = "INSERT INTO Refuge (`nationnalite`,`categorie`,`religion`) "
                + "VALUES ( ?,?,?) ";
        PreparedStatement pre = con.prepareStatement(req);
        
  pre.setString(1, ref.getNationnalite()); 
        pre.setString(2, ref.getCategorie());
           
        pre.setString(3, ref.getReligion());
       
        // pre.setString(5, C.getMail());
    
   pre.executeUpdate();
     }
}
