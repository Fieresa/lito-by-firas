/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.IService;
import Lito.Entite.User;
import java.sql.SQLException;
import java.util.List;
/**
 *
 * @author Meriem
 */
public interface IService<T> {
    void ajouter(T t) throws SQLException;
    boolean delete(T t) throws SQLException;
    boolean update(String nom,String prenom,String mail,String password,String role,User u) throws SQLException;
    List<T> readAll() throws SQLException;
    public List<User> searchUser(String n) throws SQLException;
    
}

