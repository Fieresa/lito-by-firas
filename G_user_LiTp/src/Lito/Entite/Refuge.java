/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Entite;

/**
 *
 * @author Meriem
 */
public class Refuge extends User {
       private int idR; 
    private String nationnalite; 
    private String categorie;
    private String religion; 
    
    
    
    public Refuge(){}

    public Refuge( String nationnalite, String categorie,String religion) {
    
        this.nationnalite = nationnalite;
        this.categorie = categorie;
        this.religion= religion;
    
    }

    public Refuge(int idR, String nationnalite, String categorie, int id, String nom, String prenom, String mail, String password, String role) {
        super(id, nom, prenom, mail, password, role);
        this.idR = idR;
        this.nationnalite = nationnalite;
        this.categorie = categorie;
    }

    public Refuge(int idR, String nationnalite, String categorie, String nom, String prenom, String mail, String password, String role) {
        super(nom, prenom, mail, password, role);
        this.idR = idR;
        this.nationnalite = nationnalite;
        this.categorie = categorie;
    }

    public Refuge(int idR, String nationnalite, String categorie, String mail) {
        super(mail);
        this.idR = idR;
        this.nationnalite = nationnalite;
        this.categorie = categorie;
    }

    public void setIdR(int idR) {
        this.idR = idR;
    }

    public void setNationnalite(String nationnalite) {
        this.nationnalite = nationnalite;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdR() {
        return idR;
    }

    public String getNationnalite() {
        return nationnalite;
    }

    public String getCategorie() {
        return categorie;
    }

    public String getReligion() {
        return religion;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    @Override
    public String toString() {
        return "Refuge{" + "idR=" + idR + ", nationnalite=" + nationnalite + ", categorie=" + categorie + ", religion=" + religion + '}';
    }
 
    
     
}
