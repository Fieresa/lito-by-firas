/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Entite;

import java.sql.Date;

/**
 *
 * @author Meriem
 */
public class Candidat extends User{
    

   private String adresse;
    private String CIN;
    private String telephone;
     private String niveau;
     private String critere ;
    
    public Candidat(int id,String nom, String prenom, String mail, String password, String role,  String adresse, String CIN, String telephone, String niveau, String critere) {
       super(id, nom,prenom, mail, password, role);
       
      this.adresse = adresse;
        this.CIN = CIN;
        this.telephone = telephone;
        this.niveau = niveau;
        this.critere = critere;
    
      
    }
   
    public  Candidat( String adresse,String CIN, String telephone, String niveau, String critere) {
         this.adresse = adresse;
        this.CIN = CIN;
        this.telephone= telephone;
        this.niveau = niveau;
        this.critere = critere;
      
    }
     


   
    public void setAdresse(String adresse) {
        this.adresse= adresse;
    }
    public void setCIN(String CIN) {
        this.CIN = CIN;
    }

    public void setTel(String tel) {
        this.telephone= tel;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public void setCritere(String critere) {
        this.critere = critere;
    }

 
   public String getAdresse(){
       return adresse;
   }

    public String getCIN() {
        return CIN;
    }

    public String getTel() {
        return telephone;
    }

    public String getNiveau() {
        return niveau;
    }

    public String getCritere() {
        return critere;
    }
    
    @Override
    public String toString() {
            String str = super.toString() + "\n \t ==>>" +this.CIN+ this.adresse+this.telephone+ this. niveau +  this.critere +  '}';
         return str;
    }

  
     
     
     
}
