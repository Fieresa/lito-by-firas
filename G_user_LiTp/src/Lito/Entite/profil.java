/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Entite;

/**
 *
 * @author Meriem
 */
public class profil{
        private int id;
    private int user_id;
    private String profilePicURL;
    private String timelinePicURL;

    public profil(int id, int user_id, String profilePicURL, String timelinePicURL) {
        this.id = id;
        this.user_id = user_id;
        this.profilePicURL = profilePicURL;
        this.timelinePicURL = timelinePicURL;
    }

    public profil() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public void setProfilePicURL(String profilePicURL) {
        this.profilePicURL = profilePicURL;
    }

    public String getTimelinePicURL() {
        return timelinePicURL;
    }

    public void setTimelinePicURL(String timelinePicURL) {
        this.timelinePicURL = timelinePicURL;
    }
}
