/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Entite;

/**
 *
 * @author Meriem
 */
public class Donneur extends User {
    
       private int idD; 
    private String rank; 
    private String point; 
    private String telephone; 
    public Donneur( int id, String nom, String prenom, String mail,String password, String role, String rank, String point, String telephone) {
          super(id, nom,prenom, mail, password, role);
       
        this.rank = rank;
        this.point = point;
        this.telephone = telephone;
    }
    public Donneur(){
    }

    public Donneur(String rank, String point, String telephone) {
        this.rank = rank;
        this.point = point;
        this.telephone = telephone;
    }

    

    public int getIdD() {
        return idD;
    }

    public String getRank() {
        return rank;
    }

    public String getPoint() {
        return point;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setIdD(int idD) {
        this.idD = idD;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
 @Override
    public String toString() {
            String str = super.toString() + "\n \t ==>>" +this.rank+ this.point+this.telephone+  '}';
         return str;
    }

  
    
}
