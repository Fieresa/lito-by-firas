/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lito.Entite;

import java.util.logging.Logger;

/**
 *
 * @author Meriem
 */
public class Docteur extends User {

    private int idDoc; 
    private String telephone;
    private String specialite;
    private String adresse; 

    public Docteur(int idDoc, String telephone, String specialite, String adresse) {
        this.idDoc = idDoc;
        this.telephone = telephone;
        this.specialite = specialite;
        this.adresse = adresse;
    }

    public Docteur(int idDoc, String telephone, String specialite, String adresse, int id, String nom, String prenom, String mail, String password, String role) {
        super(id, nom, prenom, mail, password, role);
        this.idDoc = idDoc;
        this.telephone = telephone;
        this.specialite = specialite;
        this.adresse = adresse;
    }

    public Docteur(int idDoc, String telephone, String specialite, String adresse, String nom, String prenom, String mail, String password, String role) {
        super(nom, prenom, mail, password, role);
        this.idDoc = idDoc;
        this.telephone = telephone;
        this.specialite = specialite;
        this.adresse = adresse;
    }

    public Docteur(int idDoc, String telephone, String specialite, String adresse, String mail) {
        super(mail);
        this.idDoc = idDoc;
        this.telephone = telephone;
        this.specialite = specialite;
        this.adresse = adresse;
    }
 public Docteur(String telephone, String specialite, String adresse) {
        

        this.telephone = telephone;
        this.specialite = specialite;
        this.adresse = adresse;
    }
 public Docteur(){}

    public int getIdDoc() {
        return idDoc;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getSpecialite() {
        return specialite;
    }

    public String getAdresse() {
        return adresse;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setIdDoc(int idDoc) {
        this.idDoc = idDoc;
    }

   
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    private static final Logger LOG = Logger.getLogger(Docteur.class.getName());
     @Override
    public String toString() {
        return "Docteur{" + "idDoc=" + idDoc + ", telephone=" + telephone + ", specialite=" + specialite + ", adresse=" + adresse + '}';
    }

}
