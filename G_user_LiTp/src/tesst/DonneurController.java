/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesst;

import Lito.Entite.Donneur;
import Lito.Entite.User;
import Lito.Service.mailService;
import Lito.Service.ServiceDonneur;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Meriem
 */
public class DonneurController implements Initializable {
      ServiceDonneur SerD= new ServiceDonneur();
private User u; 
    @FXML
    private ImageView ImgProfil;

    public void setU(User u) {
        this.u = u;
    }

    public User getU() {
        return u;
    }
    @FXML
    private TextField textRank;
    @FXML
    private TextField textTelephone;
    @FXML
    private TextField textPoint;
    @FXML
    private Button btnValider;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void ActionDonneur(ActionEvent event) throws SQLException {
         String rank = textRank.getText();
        String  point = textPoint.getText();
        String telephone = textTelephone.getText();
        
        
         Donneur d= new Donneur(rank,point,telephone);
          SerD.ajouter(d);
          
      /*String to = d.getMail(); 
             String subject = "RE: Candidature";
             String message = "Votre cnadidature a éte enregistrer ";
             String user = "cthulu0011@gmail.com";
             String pass = "@abcd111";
             mailService.send(to,subject, message, user, pass);  */
     
    }   
      @FXML
      private void profilAction(MouseEvent event) throws IOException {
          FXMLLoader f = new FXMLLoader(getClass().getResource("/tesst/profil.fxml"));
                Parent root2 = (Parent) f.load();
                 ProfilController r = f.<ProfilController>getController();
                 r.setU(u);
                btnValider.getScene().setRoot(root2);
        
    }

}

   
    
