/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesst;

import Lito.Entite.User;
import Lito.Service.ServiceUser;
import Lito.Service.mailService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Meriem
 */
public class RegisterController implements Initializable {
    ServiceUser su = new ServiceUser();
    @FXML
    private ComboBox<String> textRole;
    @FXML
    private Button regiter;
    @FXML
    private TextField textNom;
    @FXML
    private TextField textMail;
    @FXML
    private TextField textPrenom;
    @FXML
    private PasswordField textPassword;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         ObservableList<String> list = FXCollections.observableArrayList("Admin","donneur","candidat","recuteur","docteur","refuge");
           textRole.setItems(list);
        textRole.getSelectionModel().select(0);
         
    }    

    @FXML
    private void registerAction(ActionEvent event) {
       String nom = textNom.getText();
        String prenom = textPrenom.getText();
          String mail = textMail.getText();
          String password = textPassword.getText();
          String Role = textRole.getSelectionModel().getSelectedItem().toString();
          User u = new User(nom,prenom,mail,password,Role);
          su.registerC(u);
           String to = u.getMail();
             String subject = "Creation Compte";
             String message = "Votre compte a été créé";
             String user = "cthulu0011@gmail.com";
             String pass = "@abcd111";
             mailService.send(to,subject, message, user, pass);
          
    }
    
}
