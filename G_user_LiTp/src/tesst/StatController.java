/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesst;

//import javax.mail.Service.DonService;
import Lito.Service.ServiceCandidat;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.chart.PieChart;

/**
 * FXML Controller class
 *
 * @author Administrateur
 */
public class StatController implements Initializable {
@FXML 
private PieChart pChart;
  
                ServiceCandidat SerC = new ServiceCandidat();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    float i =0;
        try {
         i = SerC.stat();
    } catch (SQLException ex) {
        Logger.getLogger(StatController.class.getName()).log(Level.SEVERE, null, ex);
    }
                       
        ObservableList<PieChart.Data> PieData = FXCollections.observableArrayList(
new PieChart.Data("benevole"+" "+i+"%", i),
new PieChart.Data("association"+" "+(100-i)+"%", 100-i)
);

pChart.setData(PieData);

        // TODO
    }    
 /*                      @FXML
private void go_acceuil(ActionEvent event) {
         FXMLLoader loader = new FXMLLoader
                        (getClass()
                         .getResource("user.fxml"));
          try {
                Parent root = loader.load();
                UserfxmlController y = loader.getController();
                pChart.getScene().setRoot(root);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
                };
        
        }*/

/*@FXML
 private void go_deconnexion(ActionEvent event) {
         FXMLLoader loader = new FXMLLoader
                        (getClass()
                         .getResource("FXMLDocument.fxml"));
          try {
                Parent root = loader.load();
                FXMLDocumentController y = loader.getController();
                pChart.getScene().setRoot(root);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
                };
        
        }
@FXML
private void go_retour(ActionEvent event) {
         FXMLLoader loader = new FXMLLoader
                        (getClass()
                         .getResource("user.fxml"));
          try {
                Parent root = loader.load();
                UserfxmlController y = loader.getController();
                pChart.getScene().setRoot(root);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
                };
        
        }*/

    @FXML
    private void go_deconnexion(ActionEvent event) {
    }

    @FXML
    private void go_retour(ActionEvent event) {
    }

    @FXML
    private void go_acceuil(ActionEvent event) {
    }
    
}
