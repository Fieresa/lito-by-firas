/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesst;

import Lito.Entite.Docteur;
import Lito.Entite.User;
import Lito.Service.ServiceDocteur;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Meriem
 */
public class DocteurController implements Initializable {
          ServiceDocteur SerDoc= new ServiceDocteur();
private User u; 

    public void setU(User u) {
        this.u = u;
    }

    public User getU() {
        return u;
    }

    @FXML
    private TextField textTelephone;
    @FXML
    private TextField TextSpecialite;
    @FXML
    private TextField TextAdresse;
    @FXML
    private Button btnValider;
    @FXML
    private ImageView ProfilImg;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void ValiderDonneurAction(ActionEvent event) throws SQLException {
          String telephone = textTelephone.getText();
        String  specialite = TextSpecialite.getText();
        String adresse = TextAdresse.getText();
        
        
         Docteur doc= new Docteur(telephone,specialite,adresse);
          SerDoc.ajouter(doc);   
    }
    @FXML
    private void profilAction(MouseEvent event) throws IOException {
          FXMLLoader f = new FXMLLoader(getClass().getResource("/tesst/profil.fxml"));
                Parent root2 = (Parent) f.load();
                 ProfilController r = f.<ProfilController>getController();
                 r.setU(u);
                ProfilImg.getScene().setRoot(root2);
        
    }
    
}
