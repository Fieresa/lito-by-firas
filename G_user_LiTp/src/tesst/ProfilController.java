/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesst;

import Lito.Entite.User;
import Lito.Service.ServiceUser;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Meriem
 */
public class ProfilController implements Initializable {
    private User u;
    @FXML
    private Button show;

    public User getU() {
        return u;
    }

    public void setU(User u) {
        this.u = u;
    }
    ServiceUser su= new ServiceUser();
    @FXML
    private TextField textNom;
    @FXML
    private TextField textPrenom;
    @FXML
    private TextField textMail;
    @FXML
    private TextField textPassword;
    @FXML
    private TextField textRole;
    @FXML
    private Button modifier;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    
    }    

    @FXML
    private void ModifAction(ActionEvent event) throws SQLException {
       String nom = textNom.getText();
        String prenom = textPrenom.getText();
          String mail = textMail.getText();
          String password = textPassword.getText();
          //User u = new User(nom,prenom,mail,password,textRole.getText());
          su.update(nom, prenom, mail, password,textRole.getText() , u);
          

    }
    

    @FXML
    public void affiche(){
 textNom.setText(u.getNom());

 textPrenom.setText(u.getPrenom());

 textMail.setText(u.getMail());

 textPassword.setText(u.getPassword());

 textRole.setText(u.getRole());
}
}