/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesst;

import Lito.Entite.Refuge;
import Lito.Entite.User;
import Lito.Service.ServiceRefuge;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Meriem
 */
public class RefugeController implements Initializable {
            ServiceRefuge SerRef = new ServiceRefuge();
private User u; 
  

    public void setU(User u) {
        this.u = u;
    }

    public User getU() {
        return u;
    }

    @FXML
    private TextField textNationnalite;
    @FXML
    private TextField TextCategorie;
    @FXML
    private TextField TextReligion;
    @FXML
    private Button ValiderRefugebtn;
    @FXML
    private ImageView profilImg;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            
        // TODO
    }    

    @FXML
    private void ValiderRefugeAction(ActionEvent event) throws SQLException {
         String nationnalite = textNationnalite.getText();
        String categorie = TextCategorie.getText();
        String religion = TextReligion.getText();
     
        
       
         
               Refuge ref= new Refuge(nationnalite,categorie,religion);
          SerRef.ajouter(ref);
    }

    @FXML
    private void ProfilAction(MouseEvent event) throws IOException {
          FXMLLoader f = new FXMLLoader(getClass().getResource("/tesst/profil.fxml"));
                Parent root2 = (Parent) f.load();
                 ProfilController r = f.<ProfilController>getController();
                 r.setU(u);
                profilImg.getScene().setRoot(root2);
        
    }
    }

