/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesst;

import Lito.Entite.Candidat;
import Lito.Entite.User;
import Lito.Service.ServiceCandidat;
import Lito.Service.mailService;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import static javafx.scene.input.KeyCode.C;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Meriem
 */
public class CandidatureController implements Initializable {
        ServiceCandidat SerC = new ServiceCandidat();
private User u; 
    @FXML
    private ImageView profilImg;

    public void setU(User u) {
        this.u = u;
    }

    public User getU() {
        return u;
    }
    @FXML
    private TextField textAdresse;
    @FXML
    private TextField textCIN;
    @FXML
    private TextField textTelephone;
    @FXML
    private TextField textNivau;
    @FXML
    private ComboBox<String> textCritere;
    @FXML
    private Button btnvalider;
 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
           ObservableList<String> list = FXCollections.observableArrayList("benevole","Association");
           textCritere.setItems(list);
        textCritere.getSelectionModel().select(0);
    }    

    @FXML
    private void validerAction(ActionEvent event) throws SQLException {
      //  Date Date = java.sql.Date.valueOf(textDate.getValue());
          String adresse = textAdresse.getText();
        String CIN = textCIN.getText();
        String telephone = textTelephone.getText();
        String Niveau = textNivau.getText();
        
         String critere = textCritere.getSelectionModel().getSelectedItem().toString();
         
               Candidat c= new Candidat(adresse,CIN,telephone,Niveau,critere);
          SerC.ajouter(c);
          
     /* String to = c.getMail(); 
             String subject = "RE: Candidature";
             String message = "Votre cnadidature a éte enregistrer ";
             String user = "cthulu0011@gmail.com";
             String pass = "@abcd111";
             mailService.send(to,subject, message, user, pass);*/

          
    }

    @FXML
    private void profilAction(MouseEvent event) throws IOException {
        FXMLLoader f = new FXMLLoader(getClass().getResource("/tesst/profil.fxml"));
                Parent root2 = (Parent) f.load();
                 ProfilController r = f.<ProfilController>getController();
                 r.setU(u);
                btnvalider.getScene().setRoot(root2);
        
    }
    
}
