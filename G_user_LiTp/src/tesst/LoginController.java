/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesst;

import Lito.Entite.User;
import Lito.Service.ServiceUser;
import Lito.Utils.ConnectionUtil;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import Lito.Utils.ConnectionUtil;
import java.io.IOException;
import java.sql.SQLException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

 

public class LoginController implements Initializable {
        
    @FXML
    private TextField textEmail;
    
    @FXML
    private PasswordField textPassword;
    
    Stage dialogStage = new Stage();
    Scene scene;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    ServiceUser su= new ServiceUser();
    @FXML
    private Label reg;
 
    @FXML
    public void loginAction(ActionEvent event) throws SQLException, IOException{
        String mail = textEmail.getText();
        String password = textPassword.getText();
    

        
        try{
            /*preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, mail);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if(!resultSet.next()){
             infoBox("Please enter correct mail and Password", null, "Failed");*/
            User u=su.login(mail,password);
                 if(u==null){
                          infoBox("Login failer",null,"error" );
                Node node = (Node)event.getSource();
                dialogStage = (Stage) node.getScene().getWindow();
                dialogStage.close();
                scene = new Scene(FXMLLoader.load(getClass().getResource("loginnn.fxml")));
                dialogStage.setScene(scene);
                dialogStage.show(); 
        }
        else{
                if(u.getRole().equals("candidat")){
                infoBox("Login Successfull",null,"Success" );
                /*Node node = (Node)event.getSource();
                dialogStage = (Stage) node.getScene().getWindow();
                dialogStage.close();
                scene = new Scene(FXMLLoader.load(getClass().getResource("loginnn.fxml")));
                dialogStage.setScene(scene);
                dialogStage.show();*/
                FXMLLoader f = new FXMLLoader(getClass().getResource("/tesst/candidature.fxml"));
                Parent root2 = (Parent) f.load();
                 CandidatureController c = f.<CandidatureController>getController();
                 c.setU(u);
                reg.getScene().setRoot(root2);
                }
                else if(u.getRole().equals("donneur")){
                 infoBox("Login Successfull",null,"Success" );
                 FXMLLoader f = new FXMLLoader(getClass().getResource("/tesst/donneur.fxml"));                   
                Parent root2 = (Parent) f.load();
                 DonneurController d = f.<DonneurController>getController();
                 d.setU(u);
                reg.getScene().setRoot(root2);  
                        }  else if(u.getRole().equals("docteur")){
                 infoBox("Login Successfull",null,"Success" );
                 FXMLLoader f = new FXMLLoader(getClass().getResource("/tesst/Docteur.fxml"));                   
                Parent root2 = (Parent) f.load();
                 DocteurController d = f.<DocteurController>getController();
                 d.setU(u);
                reg.getScene().setRoot(root2);  
                        }   else if(u.getRole().equals("refuge")){
                 infoBox("Login Successfull",null,"Success" );
                 FXMLLoader f = new FXMLLoader(getClass().getResource("/tesst/Refuge.fxml"));                   
                Parent root2 = (Parent) f.load();
                 RefugeController ref = f.<RefugeController>getController();
                 ref.setU(u);
                reg.getScene().setRoot(root2);  
                        }
            }
        }
            catch(Exception e){
            e.printStackTrace();
            }
        

        }
        

    public static void infoBox(String infoMessage, String headerText, String title){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
    }    

    @FXML
    private void deplacerAction(MouseEvent event) {
        try {
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("register.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            reg.getScene().setRoot(root);
            //Stage stage = new Stage();
            //stage.setScene(new Scene(root));  
            //stage.show();
            //main.stg.close();
    } catch(IOException e) {
    }
    }

    @FXML
    private void deplacAction(MouseEvent event) {
         
               String sql = "SELECT * FROM user WHERE role = candidat";
                     try {
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("candidature.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            reg.getScene().setRoot(root);
            //Stage stage = new Stage();
            //stage.setScene(new Scene(root));  
            //stage.show();
            //main.stg.close();
    } catch(IOException e) {
    }
    }
}
     /*  public void setid(int id) {
        this.id.setText(""+id);
        this.id.setVisible(false);
    }*/

   

               
